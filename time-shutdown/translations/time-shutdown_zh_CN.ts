<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>QObject</name>
    <message>
        <location filename="../comboxwidget.cpp" line="39"/>
        <source>Shutdown</source>
        <translation>关机频率</translation>
    </message>
    <message>
        <location filename="../confirmareawidget.cpp" line="35"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../confirmareawidget.cpp" line="39"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="76"/>
        <source>never</source>
        <translation>从不</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="77"/>
        <source>Only this shutdown</source>
        <translation>仅本次关机</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="78"/>
        <source>Everyday</source>
        <translation>每天</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="79"/>
        <source>Mon</source>
        <translation>周一</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="80"/>
        <source>Tue</source>
        <translation>周二</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="81"/>
        <source>Wed</source>
        <translation>周三</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="82"/>
        <source>Thu</source>
        <translation>周四</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="83"/>
        <source>Fri</source>
        <translation>周五</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="84"/>
        <source>Sat</source>
        <translation>周六</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="85"/>
        <source>Sun</source>
        <translation>周日</translation>
    </message>
    <message>
        <location filename="../timeshowwidget.cpp" line="58"/>
        <source>hours</source>
        <translation>时</translation>
    </message>
    <message>
        <location filename="../timeshowwidget.cpp" line="64"/>
        <location filename="../widget.cpp" line="154"/>
        <source>minute</source>
        <translation>分</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="104"/>
        <location filename="../widget.cpp" line="105"/>
        <location filename="../widget.cpp" line="128"/>
        <location filename="../widget.cpp" line="254"/>
        <source>time-shutdown</source>
        <translation>定时关机</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="108"/>
        <source>more</source>
        <translation>更多</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="109"/>
        <source>minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="110"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="116"/>
        <source>Assist</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="117"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="118"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>Service &amp; Support:</source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="152"/>
        <source>next shutdown</source>
        <translation>距离下一次关机时间</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="153"/>
        <source>hour</source>
        <translation>小时</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="548"/>
        <location filename="../widget.cpp" line="560"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>time-shutdown is already running!</source>
        <translation>定时关机已经运行</translation>
    </message>
</context>
<context>
    <name>VerticalScroll_60</name>
    <message>
        <location filename="../verticalscroll60.cpp" line="271"/>
        <source>VerticalScroll_60</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.cpp" line="128"/>
        <source>Version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="234"/>
        <source>Mi&amp;nimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="237"/>
        <source>&amp;Restore</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="240"/>
        <source>&amp;Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="559"/>
        <source>The shutdown time is shorter than the current time</source>
        <translation>关机时间小于当前时间</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="547"/>
        <source>no set shutdown</source>
        <translation>未设置关机频率</translation>
    </message>
</context>
</TS>
