<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="../comboxwidget.cpp" line="39"/>
        <source>Shutdown</source>
        <translation>ཁ་གཏན་ཟློས་ཕྱོད་</translation>
    </message>
    <message>
        <location filename="../confirmareawidget.cpp" line="35"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ་</translation>
    </message>
    <message>
        <location filename="../confirmareawidget.cpp" line="39"/>
        <source>Confirm</source>
        <translation>ངོས་འཛིན་</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="76"/>
        <source>never</source>
        <translation>གཏན་ནས་མི་དགའ་བ་</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="77"/>
        <source>Only this shutdown</source>
        <translation>ཐེངས་འདི་ཁ་གཏན་པ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="78"/>
        <source>Everyday</source>
        <translation>ཉིན་ལྟར་</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="79"/>
        <source>Mon</source>
        <translation>གཟའ་ཟླ་བ་</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="80"/>
        <source>Tue</source>
        <translation>གཟའ་མིག་དམར་</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="81"/>
        <source>Wed</source>
        <translation>གཟའ་ལྷག་པའི་ཉིན་</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="82"/>
        <source>Thu</source>
        <translation>གཟའ་ཕུར་བུ།</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="83"/>
        <source>Fri</source>
        <translation>གཟའ་པ་སངས་</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="84"/>
        <source>Sat</source>
        <translation>གཟའ་སྤེན་པ།</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="85"/>
        <source>Sun</source>
        <translation>གཟའ་ཉི་མ།</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>time-shutdown is already running!</source>
        <translation>དུས་བཀག་ཁ་བཀག་འཁོར་སྐྱོད་ཟིན་།</translation>
    </message>
    <message>
        <location filename="../timeshowwidget.cpp" line="58"/>
        <source>hours</source>
        <translation>དུས་</translation>
    </message>
    <message>
        <location filename="../timeshowwidget.cpp" line="64"/>
        <location filename="../widget.cpp" line="154"/>
        <source>minute</source>
        <translation>སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="104"/>
        <location filename="../widget.cpp" line="105"/>
        <location filename="../widget.cpp" line="128"/>
        <location filename="../widget.cpp" line="254"/>
        <source>time-shutdown</source>
        <translation>དུས་བཅད་ཁ་རྒྱག</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="108"/>
        <source>more</source>
        <translation>དེ་བས་མང་བ་</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="109"/>
        <source>minimize</source>
        <translation>ཆེས་ཆུང་ཅན་</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="110"/>
        <source>close</source>
        <translation>སྒོ་རྒྱག་པ་</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="116"/>
        <source>Assist</source>
        <translation>རོགས་རམ།</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="117"/>
        <source>About</source>
        <translation>སྐོར།</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="118"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="152"/>
        <source>next shutdown</source>
        <translation>ཐེངས་རྗེས་མའི་ཁ་གཏན་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="153"/>
        <source>hour</source>
        <translation>ཆུ་ཚོད།</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="548"/>
        <location filename="../widget.cpp" line="560"/>
        <source>OK</source>
        <translation>གཏན་ཁེལ་</translation>
    </message>
</context>
<context>
    <name>VerticalScroll_60</name>
    <message>
        <location filename="../verticalscroll60.cpp" line="271"/>
        <source>VerticalScroll_60</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.cpp" line="128"/>
        <source>Version: </source>
        <translation>པར་གཞི།：</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="234"/>
        <source>Mi&amp;nimize</source>
        <translation>ཆེས་ཆུང་ཅན་</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="237"/>
        <source>&amp;Restore</source>
        <translation>ཁ་འབྱེད་</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="240"/>
        <source>&amp;Quit</source>
        <translation>ཕྱིར་འབུད་</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="547"/>
        <source>no set shutdown</source>
        <translation>མ་བཙུགས་པའི་ཁ་གཏན་ཟློས་ཕྱོད་</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="559"/>
        <source>The shutdown time is shorter than the current time</source>
        <translation>ཁ་གཏན་དུས་ཚོད་མིག་སྔའི་དུས་ཚོད་ལས་ཆུང་།</translation>
    </message>
</context>
</TS>
